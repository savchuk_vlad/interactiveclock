// Таймери

#include <GyverTimer.h> // для роботи таймерів

GTimer_ms screenUpdateTimer(1000); // таймер оновлення екрану (1000мс = кожну 1с)
GTimer_ms weatherUpdateTimer(60000*5); // таймер оновлення погоди з серверу (60000*5мс = кожні 5хв)
GTimer_ms weatherScreenChangeTimer(3000); // таймер перемикання екранів погоди (3000мс = кожні 3с)
GTimer_ms currenciesUpdateTimer(60000*30); // таймер оновлення курсів валют з серверу (60000*30мс = кожні 30хв)
GTimer_ms currenciesScreenChangeTimer(3000); // таймер перемикання екранів курсів валют (3000мс = кожні 3с)
GTimer_ms airPollutionUpdateTimer(60000*5); // таймер оновлення індексу якості повітря з серверу (60000*5мс = кожні 5хв)
