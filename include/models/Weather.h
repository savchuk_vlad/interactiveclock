// Структура погоди

#ifndef WEATHER_H
#define WEATHER_H

#include "Arduino.h" // необхідна для використання типу String

struct Weather 
{
    double temp; // температура повітря в градусах Цельсія
    const char* description; // короткий опис погоди
    double temp_min; // мінімальна температура
    double temp_max; // максимальна температура
    int pressure; // атмосферний тиск в мм.рт.ст.
    int humidity; // вологість повітря
    int wind_speed; // швидкість вітру в м/с
    String sunrise; // схід сонця
    String sunset; // захід сонця
} ;

#endif





