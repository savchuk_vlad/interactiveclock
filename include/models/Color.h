// Структура кольору

#ifndef COLOR_H
#define COLOR_H

struct Color
{
    int red;
    int green;
    int blue;
    
    Color(int red, int green, int blue){
        this->red = red;
        this->green = green;
        this->blue = blue;
    }
};
 
#endif