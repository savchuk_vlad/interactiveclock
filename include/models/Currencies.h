// Структура курсів валют

#ifndef CURRENCIES_H
#define CURRENCIES_H

struct Currencies
{
    double buy_USD; // купівля доллара США
    double sale_USD; // продаж доллара США
    double buy_EUR; // купівля євро
    double sale_EUR; // продаж євро
    double buy_RUR; // купівля рубля
    double sale_RUR; // продаж рубля
    double buy_BTC; // купівля біткойна
    double sale_BTC; // продаж біткойна
} ;

#endif