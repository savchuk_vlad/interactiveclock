// Структура забруднення повітря

#ifndef AIRPOLLUTION_H
#define AIRPOLLUTION_H

struct AirPollution
{
    const char* city; // місто
    int aqi; // AQI - Air Quality Index (індекс якості повітря)
} ;

#endif