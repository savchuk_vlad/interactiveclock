// Перелічення

#ifndef ENUMS
#define ENUMS

enum Screens{ // екрани часу, погоди, кімнатної температури та вологості, курсів валют, забрудення повітря
    TimeScreen, WeatherScreen, InsideScreen, CurrenciesScreen, AirPollutionScreen
};
enum WeatherScreens{ // екрани погоди: напис Weather, опис, температура та вологість, тиск та швидкість вітру, схід та захід 
    WeatherSign, Description, TempHum, PressWind, Sun
};
enum CurrenciesScreens{ // екрани курсів валют: доллар, евро, рубль, біткойн
    CurrenciesSign, USD, EUR, RUR, BTC
};

#endif