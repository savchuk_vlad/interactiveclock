// Клас мережевих запитів

#ifndef SERVERREQUEST_H
#define SERVERREQUEST_H

#include "Arduino.h"
#include <ESP8266WiFi.h>

class ServerRequest
{
private:
    WiFiClientSecure _wifiClient; // клієнт для роботи з мережею
    const String CITY = "Kyiv"; // місто, по якому будуть отримуватись дані

    const String WEATHER_API_KEY = "a1cc7b269d8c64984b94a18d892d8bb2"; // API-ключ https://openweathermap.org/appid
    const String WEATHER_SERVER = "api.openweathermap.org"; // адреса серверу погоди
    const String WEATHER_HTTPS_FINGERPRINT = "6C9D1E27F1137BC7B6159013F2D02997A45B3F7E";    // цифровий відбиток сервера погоди 
                                                                                            // https://en.wikipedia.org/wiki/Public_key_fingerprint

    const String CURRENCIES_SERVER = "api.privatbank.ua"; // адреса серверу курсів валют
    const String CURRENCIES_HTTPS_FINGERPRINT = "85d7dc79590e42eea68cc6046a9fe3933a1650de"; // цифровий відбиток сервера курсів валют 

    const String AIRPOLLUSION_API_KEY = "d1137cfa-f652-4a0c-a16e-a607f21a7e45"; // API-ключ https://www.airvisual.com/air-pollution-data-api
    const String AIRPOLLUSION_SERVER = "api.airvisual.com"; // адреса серверу якості повітря
    const String AIRPOLLUSION_HTTPS_FINGERPRINT = "f319e86583ba974d5a62d3726a50bdab928be572"; // цифровий відбиток сервера якості повітря

public:
    ServerRequest(); // конструктор
    String getWeather(); // отримання даних з сервера погоди
    String getCurrencies(); // отримання даних з сервера курсів валют
    String getAirPollution(); // отримання даних з сервера якості повітря
};

#endif


