// Клас мережевого часу

#ifndef NETWORKTIME_H
#define NETWORKTIME_H

#include "Arduino.h"
#include <NTPClient.h>
#include <WiFiUdp.h>

class NetworkTime
{
private:
    WiFiUDP _ntpUDP; // UDP - User Datagram Protocol, один із протоколів в стеку TCP/IP
    NTPClient _timeClient;  // NTP - Network Time Protocol, мережевий протокол для синхронізації часу
                            // FIXME if error here, define empty constructor in NTPClient library
public:
    NetworkTime(); // конструктор
    void init(); // метод, який ініціалізує поля
    void update(); // метод, який оновлює час
    String getTime(); // метод, який отримує час
    String getDate(); // метод, який отримує дату
    static String convertDate(unsigned long epoch); // метод, який конвертує час з epoch
    static String convertTime(unsigned long epoch); // метод, який конвертує дату з epoch
};

#endif