// Клас LED-кільця

#ifndef LEDRING_H
#define LEDRING_H

#include "Adafruit_NeoPixel.h" // бібліотека для роботи з LED-стрічками
#include "models/Color.h" // імпорт структури кольору

class LedRing
{
private:
    Adafruit_NeoPixel _ring; // об'єкт Adafruit_NeoPixel
public:
    LedRing(uint16_t pin); // конструктор (pin - пін данних)
    void fillWhite(); // зафарбувати всі LEDи в білий колір
    void showProgress(int progress, Color color);   // відображення прогресу на кільці
                                                    // (progress{0, 25, 50, 75, 100} - прогрес завантаження у відсотках
                                                    // color - колір в RGB)
    void showAirPollutionStatus(int aqi);   // зафарбовує кільце відповідно до рівня забруднення повітря
                                            // https://support.airvisual.com/en/articles/3029425-what-is-aqi
};

#endif





