// Клас для парсингу JSON

#ifndef JSONPARSER_H
#define JSONPARSER_H

#include "Arduino.h"
#include "ArduinoJson.h" // бібліотека для роботи з JSON

#include "models/Weather.h" // імпорт структури погоди
#include "models/Currencies.h" // імпорт структури курсів валют
#include "models/AirPollution.h" // імпорт структури якості повітря

class JsonParser
{
    private:
        String _json; // JSON-рядок
    public:
        JsonParser(String json); // конструктор
        Weather parseWeather(); // парсинг JSON, що містить дані про погоду
        Currencies parseCurrencies(); // парсинг JSON, що містить дані про курси валют
        AirPollution parseAirPollution(); // парсинг JSON, що містить дані про забруднення повітря
};

#endif


