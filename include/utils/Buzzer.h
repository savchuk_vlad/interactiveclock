//Клас п'єзодинаміка

#ifndef BUZZER_H
#define BUZZER_H

#include "Arduino.h"

class Buzzer
{
private:
    int _pin; // пін для підключення
public:
    Buzzer(int pin); // конструктор
    void oneBeep(); // метод для відтворення одного звукового сигналу
    void threeBeeps(); // метод для відтворення трьох звукових сигналів
};

#endif





