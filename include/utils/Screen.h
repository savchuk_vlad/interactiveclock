// Клас для роботи з екранами

#ifndef SCREEN_H
#define SCREEN_H

#include "Arduino.h"
#include "Adafruit_SSD1306.h"
#include "Adafruit_GFX.h"
#include "ESP8266WiFi.h"
#include "GyverTimer.h"

#include "utils/NetworkTime.h"
#include "models/Weather.h"
#include "models/Currencies.h"
#include "models/AirPollution.h"
#include "Enums.h"

class Screen
{
private:
    Adafruit_SSD1306 _oled; // об'єкт Adafruit_SSD1306
public:
    Screen(); // конструктор
    WeatherScreens _currentWeatherScreen; // поточний екран погоди
    CurrenciesScreens _currentCurrenciesScreen; // поточний екран курсів валют
    Screens currentMainScreen; // поточний головний екран, який відображається в даний момент
    void showWifiConnecting(ESP8266WiFiClass wifi); // метод, що відображає підключення до вказаної точки доступу
    void showWifiLost(); // метод, що відображає втрату сигналу точки доступу
    void showWifiDisconnected(); // метод, що відображає відключення від точки доступу
    void showLoading(Screens screen); // метод, що відображає завантаження залежно від екрану
    void showTimeScreen(NetworkTime networkTime); // метод, що відображає екран з часом та датою
    void showWeatherScreen(Weather weather); // метод, що відображає екран з погодою
    void showCurrenciesScreen(Currencies currencies); // метод, що відображає екран з курсами валют
    void showInsideScreen(float temperature, float humidity); // метод, що відображає екран з кімнатною температурою та вологістю
    void showAirPollutionScreen(AirPollution airPollution); // метод, що відображає екран з індексом забруднення повітря
    void updateScreen(); // метод, що оновлює екран
    static int centerredText(String text); // метод, який центрує вказаний текст по осі X
};

#endif