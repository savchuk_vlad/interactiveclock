#include "utils/ServerRequest.h"

ServerRequest::ServerRequest()
{
}

String ServerRequest::getWeather(){
    String weatherJson = "";
    char fingerprintBuff[WEATHER_HTTPS_FINGERPRINT.length()+1];
    WEATHER_HTTPS_FINGERPRINT.toCharArray(fingerprintBuff, WEATHER_HTTPS_FINGERPRINT.length()+1);
    
    _wifiClient.setFingerprint(fingerprintBuff);
    Serial.println("\nStarting connection to server..."); 
    // if you get a connection, report back via serial: 
    if (_wifiClient.connect(WEATHER_SERVER, 443)) { 
        Serial.println("Connected to WEATHER server."); 
        // Make a HTTP request: 
        _wifiClient.print("GET /data/2.5/weather?");
        _wifiClient.print("q="+CITY); 
        _wifiClient.print("&units=metric");
        _wifiClient.print("&appid="+WEATHER_API_KEY);  
        _wifiClient.println();
    } else { 
        Serial.println("Unable to connect WEATHER server."); 
    } 

     
    while (_wifiClient.connected()) { 
        weatherJson = _wifiClient.readString();
        Serial.println(weatherJson);
    }
    return weatherJson;
}


String ServerRequest::getCurrencies() {
    String currenciesJson = ""; 
    char fingerprintBuff[CURRENCIES_HTTPS_FINGERPRINT.length()+1];
    CURRENCIES_HTTPS_FINGERPRINT.toCharArray(fingerprintBuff, CURRENCIES_HTTPS_FINGERPRINT.length()+1);
    
    _wifiClient.setFingerprint(fingerprintBuff);
    Serial.println("\nStarting connection to server..."); 
    // if you get a connection, report back via serial: 
    if (_wifiClient.connect(CURRENCIES_SERVER, 443)) { 
        Serial.println("Connected to CURRENCIES server."); 
        // Make a HTTP request: 
        _wifiClient.print("GET /p24api/pubinfo?json&exchange&coursid=5");  
        _wifiClient.println();
    } else { 
        Serial.println("Unable to connect CURRENCIES server."); 
    } 


    while (_wifiClient.connected()) { 
        currenciesJson = _wifiClient.readString();
        Serial.println(currenciesJson);
    }
    return currenciesJson;
}


String ServerRequest::getAirPollution(){
    String airPollutionJson = ""; 
    char fingerprintBuff[AIRPOLLUSION_HTTPS_FINGERPRINT.length()+1];
    AIRPOLLUSION_HTTPS_FINGERPRINT.toCharArray(fingerprintBuff, AIRPOLLUSION_HTTPS_FINGERPRINT.length()+1);
    
    _wifiClient.setFingerprint(fingerprintBuff);
    Serial.println("\nStarting connection to server..."); 
    // if you get a connection, report back via serial: 
    if (_wifiClient.connect(AIRPOLLUSION_SERVER, 443)) { 
        Serial.println("Connected to AIRPOLLUTION server."); 
        // Make a HTTP request: 
        _wifiClient.print("GET /v2/city?city="+CITY+"&state=kyiv&country=ukraine&key="+AIRPOLLUSION_API_KEY);  
        _wifiClient.println();
    } else { 
        Serial.println("Unable to connect AIRPOLLUTION server."); 
    } 

    while (_wifiClient.connected()) { 
        airPollutionJson = _wifiClient.readString();
        Serial.println(airPollutionJson);
    }


    return airPollutionJson;
}