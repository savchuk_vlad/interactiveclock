 #include "utils/JsonParser.h"
 #include "ArduinoJson.h"
 #include "utils/NetworkTime.h"

JsonParser::JsonParser(String json){
    _json = json;
}

Weather JsonParser::parseWeather(){
    _json.replace('[',' ');
    _json.replace(']',' ');
    _json.remove(1, _json.substring(1, _json.indexOf("\"weather")).length());
    _json.remove(_json.indexOf(",\"base"), _json.substring(_json.indexOf(",\"base"), _json.indexOf(",\"main\":{")).length());
    _json.remove(_json.indexOf(",\"visibility"), _json.substring(_json.indexOf(",\"visibility"), _json.indexOf(",\"wind")).length());
    _json.remove(_json.indexOf(",\"clouds"),_json.substring(_json.indexOf(",\"clouds"), _json.indexOf(",\"sys")).length());
    _json.remove(_json.indexOf("\"type"), _json.substring(_json.indexOf("\"type"), _json.indexOf("\"sunrise")).length());
    Serial.println("\nFormatted:\n"+_json);

    char charBuf[_json.length()+200];
    _json.toCharArray(charBuf, _json.length()+200);

    StaticJsonDocument<2048> doc;
    deserializeJson(doc, charBuf);

    Weather weather;
    weather.description = doc["weather"]["main"];
    weather.humidity = doc["main"]["humidity"];
    weather.pressure = (double)(doc["main"]["pressure"])*100/133.3224;
    weather.temp = doc["main"]["temp"];
    weather.temp_min = doc["main"]["temp_min"];
    weather.temp_max = doc["main"]["temp_max"];
    weather.wind_speed = doc["wind"]["speed"];
    weather.sunrise = NetworkTime::convertTime((unsigned long)doc["sys"]["sunrise"]+2*60*60);
    weather.sunset = NetworkTime::convertTime((unsigned long)doc["sys"]["sunset"]+2*60*60);
    return weather;
}

Currencies JsonParser::parseCurrencies(){
    StaticJsonDocument<1024> doc;
    char charBuf[_json.length()+200];
    _json.toCharArray(charBuf, _json.length()+200);
    deserializeJson(doc, charBuf);

    Currencies currencies;
    currencies.buy_USD = doc[0]["buy"];
    currencies.sale_USD = doc[0]["sale"];
    currencies.buy_EUR = doc[1]["buy"];
    currencies.sale_EUR = doc[1]["sale"];
    currencies.buy_RUR = doc[2]["buy"];
    currencies.sale_RUR = doc[2]["sale"];
    currencies.buy_BTC = doc[3]["buy"];
    currencies.sale_BTC = doc[3]["sale"];
    return currencies;
}

AirPollution JsonParser::parseAirPollution(){
    StaticJsonDocument<1024> doc;
    char charBuf[_json.length()+200];
    _json.toCharArray(charBuf, _json.length()+200);
    deserializeJson(doc, charBuf);
    
    AirPollution air;
    air.city = doc["data"]["city"];
    air.aqi = doc["data"]["current"]["pollution"]["aqius"];
    return air;
}