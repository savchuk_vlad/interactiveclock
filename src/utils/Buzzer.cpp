#include "utils/Buzzer.h"

Buzzer::Buzzer(int pin)
{
    _pin = pin;
    pinMode(_pin, OUTPUT);
}

void Buzzer::oneBeep(){
    tone(_pin, 4000);
    delay(200);
    noTone(_pin);
}

void Buzzer::threeBeeps(){
    tone(_pin, 3000);   //5.7 2.5
    delay(200);
    noTone(_pin);
    delay(200);
    tone(_pin, 3000);
    delay(200);
    noTone(_pin);
    delay(200);
    tone(_pin, 3000);
    delay(200);
    noTone(_pin);
    delay(200);
}