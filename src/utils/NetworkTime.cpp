#include "utils/NetworkTime.h"


NetworkTime::NetworkTime(){
    _timeClient = NTPClient(_ntpUDP, "ua.pool.ntp.org", 2*60*60, 100);
}

void NetworkTime::init(){
    _timeClient.begin();
}

void NetworkTime::update(){
    while(!_timeClient.update()) {
        _timeClient.forceUpdate();
    }
}

String NetworkTime::getDate(){
    return convertDate(_timeClient.getEpochTime());
}

String NetworkTime::getTime(){
    return _timeClient.getFormattedTime();
}

String NetworkTime::convertDate(unsigned long epoch){
    String d = String(day(epoch));
    if (d.length() == 1)
        d = "0"+d;

    String m = String(month(epoch));
    if (m.length() == 1)
        m = "0"+m;

    String y = String(year(epoch));

    return (d+"."+m+"."+y);
}

String NetworkTime::convertTime(unsigned long epoch){
    String h = String(hour(epoch));
    if (h.length() == 1)
        h = "0"+h;

    String m = String(minute(epoch));
    if (m.length() == 1)
        m = "0"+m;

    String s = String(second(epoch));
    if (s.length() == 1)
        s = "0"+s;
    
    return (h+":"+m+":"+s);
}
