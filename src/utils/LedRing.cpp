#include "utils/LedRing.h"

LedRing::LedRing(uint16_t pin)
{
    _ring = Adafruit_NeoPixel(8, pin, NEO_RGB + NEO_KHZ800);
    _ring.begin();
    _ring.setBrightness(10);
    _ring.show();
}


void LedRing::fillWhite()
{
    _ring.fill(_ring.Color(255,255,255),0,8);
    _ring.show();
}

void LedRing::showProgress(int progress, Color color){
    fillWhite();
    switch (progress)
    {
        case 0:
            break;
        case 25:
            _ring.setPixelColor(1, _ring.Color(color.red,color.blue,color.green));
            _ring.setPixelColor(2, _ring.Color(color.red,color.blue,color.green));
            break;
        case 50:
            _ring.setPixelColor(1, _ring.Color(color.red,color.blue,color.green));
            _ring.setPixelColor(2, _ring.Color(color.red,color.blue,color.green));
            _ring.setPixelColor(3, _ring.Color(color.red,color.blue,color.green));
            _ring.setPixelColor(4, _ring.Color(color.red,color.blue,color.green));
            break;
        case 75:
            _ring.setPixelColor(1, _ring.Color(color.red,color.blue,color.green));
            _ring.setPixelColor(2, _ring.Color(color.red,color.blue,color.green));
            _ring.setPixelColor(3, _ring.Color(color.red,color.blue,color.green));
            _ring.setPixelColor(4, _ring.Color(color.red,color.blue,color.green));
            _ring.setPixelColor(5, _ring.Color(color.red,color.blue,color.green));
            _ring.setPixelColor(6, _ring.Color(color.red,color.blue,color.green));
            break;
        case 100:
            _ring.setPixelColor(1, _ring.Color(color.red,color.blue,color.green));
            _ring.setPixelColor(2, _ring.Color(color.red,color.blue,color.green));
            _ring.setPixelColor(3, _ring.Color(color.red,color.blue,color.green));
            _ring.setPixelColor(4, _ring.Color(color.red,color.blue,color.green));
            _ring.setPixelColor(5, _ring.Color(color.red,color.blue,color.green));
            _ring.setPixelColor(6, _ring.Color(color.red,color.blue,color.green));
            _ring.setPixelColor(7, _ring.Color(color.red,color.blue,color.green));
            _ring.setPixelColor(0, _ring.Color(color.red,color.blue,color.green));
            break;
        default:
            break;
    }
    _ring.show();
}

void LedRing::showAirPollutionStatus(int aqi){
    _ring.clear();
    if (aqi < 51)
        _ring.fill(_ring.Color(0, 255, 0), 0, 8);
    else if (aqi < 101)
        _ring.fill(_ring.Color(255,255,0), 0, 8);
    else if (aqi < 151)
        _ring.fill(_ring.Color(255,128,0), 0, 8);
    else if (aqi < 201)
        _ring.fill(_ring.Color(255,0,0), 0, 8);
    else if (aqi < 301)
        _ring.fill(_ring.Color(255,0,255), 0, 8);
    else
        _ring.fill(_ring.Color(0,255,255), 0, 8);
    _ring.show();
}
