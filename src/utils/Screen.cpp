#include "utils/Screen.h"

Screen::Screen()
{
    _oled.begin(SSD1306_SWITCHCAPVCC, 0x3c);
    _oled.clearDisplay();
    _oled.setTextColor(WHITE);
    _oled.setTextSize(1);
    currentMainScreen = Screens::TimeScreen;
}

int dotsCount = 0;
GTimer_ms wifiLoadingTimer(400);
void Screen::showWifiConnecting(ESP8266WiFiClass wifi){
    while (wifi.status() != WL_CONNECTED)
    {   
        _oled.setCursor(5,15);        
        String loadingStr = "Connecting to \n "+wifi.SSID();

        if (wifiLoadingTimer.isReady())
        {
            if (dotsCount != 4)
            {
                for (int i = 0; i < dotsCount; i++)
                    loadingStr += ".";
                    dotsCount++;
            }else
                dotsCount = 0;

            _oled.clearDisplay();
            _oled.print(loadingStr);
            _oled.display();
        }
        
    }
    
    _oled.clearDisplay();
    _oled.setCursor(35, 15);
    _oled.println("CONNECTED!");
    _oled.display();
    delay(1000);
    _oled.clearDisplay();
}

void Screen::showWifiLost(){
    _oled.clearDisplay();
    _oled.setCursor(Screen::centerredText("LOST CONNECTION"), 3);
    _oled.println("LOST CONNECTION");
    _oled.setCursor(Screen::centerredText("Reconnecting..."), _oled.getCursorY()+3);
    _oled.println("Reconnecting...");
    _oled.display();
}

void Screen::showWifiDisconnected(){
    _oled.clearDisplay();
    _oled.setCursor(Screen::centerredText("DISCONNECTED"), 3);
    _oled.println("DISCONNECTED");
    _oled.setCursor(Screen::centerredText("Reconnecting..."), _oled.getCursorY()+3);
    _oled.println("Reconnecting...");
    _oled.display();         
}

void Screen::showLoading(Screens screen){
    _oled.clearDisplay();
    String loadingStr = "Updating";
    _oled.setCursor(centerredText(loadingStr), 10);        
    _oled.println(loadingStr);

    switch (screen){
        case WeatherScreen:
            _oled.setCursor(centerredText("weather..."), _oled.getCursorY()+3);        
            _oled.println("weather...");
            break;
        case CurrenciesScreen:
            _oled.setCursor(centerredText("currencies..."), _oled.getCursorY()+3);        
            _oled.println("currencies...");
            break;
        case AirPollutionScreen:
            _oled.setCursor(centerredText("AQI..."), _oled.getCursorY()+3);        
            _oled.println("AQI...");
            break;
        default:
            break;
    }

    _oled.display();
}

void Screen::showTimeScreen(NetworkTime networkTime){
    _oled.setFont();
    _oled.stopscroll();
    _oled.clearDisplay();
    _oled.setTextSize(2);
    _oled.setCursor(18,8);
    networkTime.update();
    _oled.println(networkTime.getTime());
    _oled.setTextSize(1);
    _oled.setCursor(35, _oled.getCursorY());
    _oled.print(networkTime.getDate());
    _oled.display();
}

void Screen::showWeatherScreen(Weather w){
    _oled.clearDisplay();
    int firstDotX = 50;
    String tempStr;
    String humStr;
    String pressStr;
    String windStr;
    String sunriseStr;
    String sunsetStr;

    switch (_currentWeatherScreen)
    {
        case WeatherSign:
            _oled.setCursor(centerredText("WEATHER"), 15);
            _oled.println("WEATHER");
            break;

        case Description:
            _oled.setCursor(centerredText("Today is"), 2);
            _oled.println("Today is");
            _oled.setCursor(centerredText(w.description), _oled.getCursorY());
            _oled.println(w.description);

            _oled.fillCircle(firstDotX, 29, 2, WHITE);
            _oled.drawCircle(firstDotX+10, 29, 2, WHITE);
            _oled.drawCircle(firstDotX+20, 29, 2, WHITE);
            _oled.drawCircle(firstDotX+30, 29, 2, WHITE);
            break;
        case TempHum:
            tempStr = "Temp: "+String(w.temp)+" C";
            _oled.setCursor(centerredText(tempStr), 2);
            _oled.println(tempStr);

            humStr = "Humidity: "+String(w.humidity)+" %";
            _oled.setCursor(centerredText(humStr), _oled.getCursorY()+3);
            _oled.println(humStr);

            _oled.drawCircle(firstDotX, 29, 2, WHITE);
            _oled.fillCircle(firstDotX+10, 29, 2, WHITE);
            _oled.drawCircle(firstDotX+20, 29, 2, WHITE);
            _oled.drawCircle(firstDotX+30, 29, 2, WHITE);
            break;
        case PressWind:
            pressStr = "Pressure: "+String(w.pressure)+" mm";
            _oled.setCursor(centerredText(pressStr), 2);
            _oled.println(pressStr);

            windStr = "Wind speed: "+String(w.wind_speed)+" m/s";
            _oled.setCursor(centerredText(windStr), _oled.getCursorY()+3);
            _oled.println(windStr);

            _oled.drawCircle(firstDotX, 29, 2, WHITE);
            _oled.drawCircle(firstDotX+10, 29, 2, WHITE);
            _oled.fillCircle(firstDotX+20, 29, 2, WHITE);
            _oled.drawCircle(firstDotX+30, 29, 2, WHITE);
            break;
        case Sun:
            sunriseStr = "Sunrise: "+String(w.sunrise);
            _oled.setCursor(centerredText(sunriseStr), 2);
            _oled.println(sunriseStr);

            sunsetStr = "Sunset:  "+String(w.sunset);
            _oled.setCursor(centerredText(sunsetStr), _oled.getCursorY()+3);
            _oled.println(sunsetStr);

            _oled.drawCircle(firstDotX, 29, 2, WHITE);
            _oled.drawCircle(firstDotX+10, 29, 2, WHITE);
            _oled.drawCircle(firstDotX+20, 29, 2, WHITE);
            _oled.fillCircle(firstDotX+30, 29, 2, WHITE);
            break;
        default:
            break;
    }
    
    _currentWeatherScreen = (WeatherScreens)((int)_currentWeatherScreen + 1);

    if (_currentWeatherScreen > Sun)
        _currentWeatherScreen = Description;
    
    _oled.display();
}

void Screen::showCurrenciesScreen(Currencies c){
    _oled.clearDisplay();
    int firstDotX = 50;
    String buy_USD;
    String sale_USD;
    String buy_EUR;
    String sale_EUR;
    String buy_RUR;
    String sale_RUR;
    String buy_BTC;
    String sale_BTC;

    switch (_currentCurrenciesScreen)
    {
        case CurrenciesSign:
            _oled.setCursor(centerredText("CURRENCIES"), 15);
            _oled.println("CURRENCIES");
            break;

        case USD:
            buy_USD = "Buy USD:  "+String(c.buy_USD);
            _oled.setCursor(centerredText(buy_USD), 2);
            _oled.println(buy_USD);

            sale_USD = "Sale USD: "+String(c.sale_USD);
            _oled.setCursor(centerredText(sale_USD), _oled.getCursorY()+3);
            _oled.println(sale_USD);

            _oled.fillCircle(firstDotX, 29, 2, WHITE);
            _oled.drawCircle(firstDotX+10, 29, 2, WHITE);
            _oled.drawCircle(firstDotX+20, 29, 2, WHITE);
            _oled.drawCircle(firstDotX+30, 29, 2, WHITE);
            break;

        case EUR:
            buy_EUR = "Buy EUR:  "+String(c.buy_EUR);
            _oled.setCursor(centerredText(buy_EUR), 2);
            _oled.println(buy_EUR);

            sale_EUR = "Sale EUR: "+String(c.sale_EUR);
            _oled.setCursor(centerredText(sale_EUR), _oled.getCursorY()+3);
            _oled.println(sale_EUR);

            _oled.drawCircle(firstDotX, 29, 2, WHITE);
            _oled.fillCircle(firstDotX+10, 29, 2, WHITE);
            _oled.drawCircle(firstDotX+20, 29, 2, WHITE);
            _oled.drawCircle(firstDotX+30, 29, 2, WHITE);
            break;

        case RUR:
            buy_RUR = "Buy RUB:  "+String(c.buy_RUR);
            _oled.setCursor(centerredText(buy_RUR), 2);
            _oled.println(buy_RUR);

            sale_RUR = "Sale RUB: "+String(c.sale_RUR);
            _oled.setCursor(centerredText(sale_RUR), _oled.getCursorY()+3);
            _oled.println(sale_RUR);

            _oled.drawCircle(firstDotX, 29, 2, WHITE);
            _oled.drawCircle(firstDotX+10, 29, 2, WHITE);
            _oled.fillCircle(firstDotX+20, 29, 2, WHITE);
            _oled.drawCircle(firstDotX+30, 29, 2, WHITE);
            break;
            
        case BTC:
            buy_BTC = "Buy BTC:  "+String(c.buy_BTC);
            _oled.setCursor(centerredText(buy_BTC), 2);
            _oled.println(buy_BTC);

            sale_BTC = "Sale BTC: "+String(c.sale_BTC);
            _oled.setCursor(centerredText(sale_BTC), _oled.getCursorY()+3);
            _oled.println(sale_BTC);

            _oled.drawCircle(firstDotX, 29, 2, WHITE);
            _oled.drawCircle(firstDotX+10, 29, 2, WHITE);
            _oled.drawCircle(firstDotX+20, 29, 2, WHITE);
            _oled.fillCircle(firstDotX+30, 29, 2, WHITE);
            break;

        default:
            break;
    }
    
    _currentCurrenciesScreen = (CurrenciesScreens)((int)_currentCurrenciesScreen + 1);

    if (_currentCurrenciesScreen > BTC)
        _currentCurrenciesScreen = USD;
    
    _oled.display();
}

void Screen::showInsideScreen(float temperature, float humidity){
    _oled.clearDisplay();
    _oled.setCursor(Screen::centerredText("Inside "), 2);
    _oled.println("Inside");
    _oled.setCursor(5, _oled.getCursorY()+3);
    _oled.println("Temp:     "+String(temperature)+" C");
    _oled.setCursor(5, _oled.getCursorY()+3);
    _oled.println("Humidity: "+String(humidity)+" %");
    _oled.display();
}

void Screen::showAirPollutionScreen(AirPollution airPollution){
    _oled.setFont();
    _oled.clearDisplay();
    _oled.setTextSize(2);
    _oled.setCursor(35,8);
    _oled.print(airPollution.aqi);
    _oled.setTextSize(1);
    _oled.println("AQI");
    _oled.println();
    _oled.setCursor(70, _oled.getCursorY());
    _oled.print("in "+String(airPollution.city));
    _oled.display();
}

int Screen::centerredText(String s){
    return (128-s.length()*6)/2;
}