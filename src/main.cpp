// підключення бібліотек для роботи
#include <GyverEncoder.h> // енкодера
#include <ESP8266WiFi.h> // плати ESP8266
#include "DHT.h" // датчика DHT11
#include "Adafruit_Sensor.h" // датчика DHT11

// підключення класів
#include "utils/ServerRequest.h" // для запитів до серверів погоди, курсів валют і тд.
#include "utils/JsonParser.h" // для парсингу отриманих з серверу JSON-даних 
#include "utils/Buzzer.h" // для роботи з п'єзодинаміком
#include "utils/Screen.h" // для роботи з екранами
#include "utils/LedRing.h" // для роботи з LED-кільцем
#include "Enums.h" // необхідні переліченння
#include "Timers.h" // необхідні таймери

// оголошення пінів енкодера
#define CLK D5
#define DT D3
#define SW D4

#define BUILTIN_LED D4 // пін вбудованого LED
#define RING_PIN D8 // пін данних для LED-кільця
#define BUZZER_PIN D7 // пін п'єзодинаміка
#define DHT_PIN D6 // пін датчика DHT11


const String SSID = "hello";  // назва точки доступу Wi-Fi
const String PASSWORD = "10101010240101"; // пароль точки доступу

Encoder enc(CLK, DT, SW, TYPE2); // створення об'єкту енкодера
DHT dht(DHT_PIN, 11); // створення об'єкту датчика температури та вологості
Buzzer buzzer(BUZZER_PIN); // створення об'єкту п'єзодинаміка
LedRing ledRing(D8); // створення об'єкту LED-кільця

Screen screen; // створення об'єкту екрану
Weather weather; // створення об'єкту погоди
Currencies currencies; // створення об'єкту курсів валют
AirPollution airPollution; // створення об'єкту індексу забрудненості повітря

NetworkTime networkTime; // створення об'єкту мережевого часу
ServerRequest serverRequest; // створення об'єкту запиту до сервера

void checkConnection(){ // метод для перевірки з'єднання з мережею Wi-Fi
    while(WiFi.status() != WL_CONNECTED){
        switch (WiFi.status())
        {
            case WL_CONNECTION_LOST:
                screen.showWifiLost();
                break;
            case WL_DISCONNECTED:
                screen.showWifiDisconnected();     
                break;
            default:
                break;
        }
    }
}

void updateScreen(){ // метод оновлення поточного екрану
    switch (screen.currentMainScreen)
    {
        case TimeScreen:
            screen.showTimeScreen(networkTime);
            ledRing.showProgress(0, Color(50,200,255));
            break;

        case WeatherScreen:
            if (weatherUpdateTimer.isReady()){
                screen.showLoading(WeatherScreen);
                weather = JsonParser(serverRequest.getWeather()).parseWeather();
            }

            if (weatherScreenChangeTimer.isReady())
                screen.showWeatherScreen(weather);
            ledRing.showProgress(25, Color(50,200,255));
            break;

        case InsideScreen:
            screen.showInsideScreen(dht.readTemperature(), dht.readHumidity());
            ledRing.showProgress(50, Color(50,200,255));
            break;

        case CurrenciesScreen:
            ledRing.showProgress(75, Color(50,200,255));
            if (currenciesUpdateTimer.isReady()){
                screen.showLoading(CurrenciesScreen);
                currencies = JsonParser(serverRequest.getCurrencies()).parseCurrencies();
            }

            if (currenciesScreenChangeTimer.isReady())
                screen.showCurrenciesScreen(currencies);;
            break;
            
        case AirPollutionScreen:
            screen.showAirPollutionScreen(airPollution);
            if (airPollutionUpdateTimer.isReady()){
                screen.showLoading(AirPollutionScreen);
                airPollution = JsonParser(serverRequest.getAirPollution()).parseAirPollution();
            }       
            ledRing.showAirPollutionStatus(airPollution.aqi);
            break;
            
        default:
            break;
    }
}

void readEncoderState(){ // метод, який зчитує взаємодію з енкодером
    Screens previousScreen = screen.currentMainScreen;
    enc.tick();

    if (enc.isRight())
        screen.currentMainScreen = (Screens)((int)screen.currentMainScreen + 1);
    
    if (enc.isLeft())
        screen.currentMainScreen = (Screens)((int)screen.currentMainScreen - 1);
    
    if (enc.isTurn())
    {
        weatherScreenChangeTimer.start();
        screen._currentWeatherScreen = WeatherSign;
        currenciesScreenChangeTimer.start();
        screen._currentCurrenciesScreen = CurrenciesSign;
        buzzer.oneBeep();
    }
    
    if (screen.currentMainScreen < 0)
        screen.currentMainScreen = (Screens)4;
    else
    if (screen.currentMainScreen > 4)
        screen.currentMainScreen = (Screens)0;
        
    if (previousScreen != screen.currentMainScreen){
        updateScreen();
    }    
}

void setup() { 
    Serial.begin(115200); // ініціалізація серійного порту для сервісної інформації
    enc.setDirection(REVERSE); // встановлення зворотнього ходу для енкодера (необхідно залежно від типу енкодера)
    dht.begin(); // ініціалізація датчика температури та вологості

    WiFi.mode(WIFI_STA); // встановлення режиму роботи Wi-Fi модуля
    WiFi.begin(SSID, PASSWORD); // ініціалізація підключення до Wi-Fi мережі
    WiFi.setAutoConnect(true); // вмикаємо автопідключення
    ledRing.showProgress(25, Color(0,255,0)); // відображення першого етапу завантаження на LED-кільці
    screen.showWifiConnecting(WiFi); // відображення відповідного напису на екрані

    networkTime.init(); // ініціалізація мережевого часу
    buzzer.oneBeep(); // один звуковий сигнал п'єзодинаміка
    
    screen.showLoading(WeatherScreen); // відображення відповідного напису на екрані
    ledRing.showProgress(50, Color(0,255,0)); // відображення другого етапу завантаження на LED-кільці
    weather = JsonParser(serverRequest.getWeather()).parseWeather(); // надсилання запиту на сервер погоди, отримання результату, парсинг та запис в об'єкт Weather
    buzzer.oneBeep(); // один звуковий сигнал п'єзодинаміка
    
    screen.showLoading(CurrenciesScreen); // відображення відповідного напису на екрані
    ledRing.showProgress(75, Color(0,255,0)); // відображення третього етапу завантаження на LED-кільці
    currencies = JsonParser(serverRequest.getCurrencies()).parseCurrencies(); // надсилання запиту на сервер курсів валют, отримання результату, парсинг та запис в об'єкт Currencies
    buzzer.oneBeep(); // один звуковий сигнал п'єзодинаміка
    
    screen.showLoading(AirPollutionScreen); // відображення відповідного напису на екрані
    ledRing.showProgress(100, Color(0,255,0)); // відображення четвертого етапу завантаження на LED-кільці
    airPollution = JsonParser(serverRequest.getAirPollution()).parseAirPollution(); // надсилання запиту на сервер якості повітря, отримання результату, парсинг та запис в об'єкт AirPollution
    
    buzzer.threeBeeps(); // три звукових сигнали п'єзодинаміка
}

void loop() {
 checkConnection(); // перевірка статусу з'єднання
 readEncoderState(); // зчитування стану енкодера

 if (screenUpdateTimer.isReady()) // якщо настав час оновлення екрану - оновлюємо його
     updateScreen();      
}

