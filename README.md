InteractiveClock project developed by Vlad Savchuk.
For NodeMCU ESP8266.

Project structure.

interactiveclock
├─ .pio
├─ .vscode
├─ include
│  ├─ Enums.h
│  ├─ models
│  │  ├─ AirPollution.h
│  │  ├─ Color.h
│  │  ├─ Currencies.h
│  │  └─ Weather.h
│  ├─ Timers.h
│  └─ utils
│     ├─ Buzzer.h
│     ├─ JsonParser.h
│     ├─ LedRing.h
│     ├─ NetworkTime.h
│     ├─ Screen.h
│     └─ ServerRequest.h
├─ lib
│  ├─ Adafruit-GFX-Library-1.6.1
│  ├─ Adafruit_NeoPixel-1.2.5
│  ├─ Adafruit_Sensor-1.0.2
│  ├─ Adafruit_SSD1306-2.0.1
│  ├─ ArduinoJson
│  ├─ DHT-sensor-library-1.3.6
│  ├─ GyverEncoder
│  ├─ GyverTimer
│  ├─ NTPClient-3.2.0
│  └─ Time-1.5
├─ platformio.ini
├─ src
│  ├─ main.cpp
│  └─ utils
│     ├─ Buzzer.cpp
│     ├─ JsonParser.cpp
│     ├─ LedRing.cpp
│     ├─ NetworkTime.cpp
│     ├─ Screen.cpp
│     └─ ServerRequest.cpp
└─ test
   └─ README
